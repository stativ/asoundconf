#!/usr/bin/env python2

import os, sys

from distutils.core import setup

data = [ ('bin', ['asoundconf', 'asoundconf-gtk/asoundconf-gtk']),
         ('share/man/man1', ['asoundconf.1']),
         ('share/applications', ['asoundconf-gtk.desktop']) ]

setup(name= "asoundconf-gtk",
	version= "1.1",
	license= "GPL",
	description= "Allows you to select the default ALSA sound card",
	author= "Toby Smithe, Daniel T Chen, Lukas Jirkovsky",
	author_email= "l.jirkovsky@gmail.com",
	url= "https://bitbucket.org/stativ/asoundconf",
	packages= [ 'asoundconf-gtk' ],
	py_modules = ['asoundconf_common'],
	data_files= data
	)
